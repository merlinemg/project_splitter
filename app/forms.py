from django import forms


class DocumentForm(forms.Form):
    files = forms.FileField(widget=forms.ClearableFileInput(attrs={'multiple': True}))
    exclude = forms.IntegerField(widget=forms.TextInput(), initial=0)