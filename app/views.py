import datetime
import json
import csv
import os
from operator import itemgetter
from django.http import HttpResponse, Http404
from django.shortcuts import render, redirect
from django.template.loader import render_to_string
from django.utils.encoding import smart_str

from django.views.generic import TemplateView
import csv
from app.forms import DocumentForm
from app.models import File, Column, Row, Type
from django.core import serializers

from spliter import settings
from spliter.settings import BASE_DIR


class HomeView(TemplateView):
    template_name = 'file_upload.html'
    form_class = DocumentForm


    def get_context_data(self, **kwargs):
        context = super(HomeView, self).get_context_data(**kwargs)
        context['form'] = self.form_class()

        file_array = []
        data_array = []
        row_dict = {}
        for files in File.objects.all():
            column_array = []
            if len(Column.objects.filter(file_column=files)) != 0:
                range_of_rows = range(0,len(Row.objects.filter(row_value__file_column=files))/len(Column.objects.filter(file_column=files)))
                for no_row in range_of_rows:
                    temp_1 = []
                    data_array = []
                    for column in Column.objects.filter(file_column=files):
                        if column.id not in temp_1:
                            temp_1.append(column.id)
                            data_array.append(Row.objects.filter(row_value=column)[no_row])

                    column_array.append(data_array)
            file_array.append({'files':files,'range':range_of_rows,'columns':Column.objects.filter(file_column=files),'rows':column_array})
        # Column.objects.all().delete()
        # File.objects.all().delete()
        # Row.objects.all().delete()
        print file_array
        context['files'] = file_array
        return context
    def post(self, request, *args, **kwargs):

        form = DocumentForm(request.POST, request.FILES)
        exclude = request.POST.get('exclude')
        if exclude:
            counter = int(exclude)

            for document_file in form.files.getlist('files'):
                reader = csv.reader(document_file)
                row_counter = 0
                file_obj = File()
                file_obj.filename = str(document_file)
                file_obj.save()
                column_obj = []
                temp = 0
                for row in reader:
                    if temp == 0:
                        for column_counter in range(0,len(row)):
                            c_obj = Column()
                            column_obj.append(c_obj)
                            column_obj[column_counter].save()
                            file_obj.columns.add(column_obj[column_counter])
                            temp += 1
                    if row_counter >= counter:
                        data_counter = 0
                        for column in row:
                            row_obj = Row()
                            row_obj.value = column
                            row_obj.save()
                            column_obj[data_counter].row_data.add(row_obj)
                            try:
                                int(column)
                                flag = "integer"
                            except:
                                try:
                                    float(column)
                                    flag = "float field"
                                except:
                                    try:
                                        datetime.datetime.strptime(column, '%m/%d/%Y')
                                        flag= "datefield"
                                    except:
                                        str(column)
                                        flag = "string"
                            column_obj[data_counter].types = Type.objects.get(type=flag)
                            column_obj[data_counter].save()
                            data_counter += 1
                    row_counter += 1
                for column_counter in range(0, len(row)):
                    column_obj[column_counter].no_of_rows = row_counter
                    column_obj[column_counter].save()
        method_type = request.POST.get('method_type')
        if request.is_ajax():
            response_data = {}
            if method_type == "get-type":
                selected_type = request.POST.get('id')
                column_id = request.POST.get('selectid')
                column = Column.objects.get(id=column_id)
                try:
                    sel_type = Type.objects.get(type=selected_type)
                    column.types =sel_type
                    column.save()
                    response_data['status'] = True
                    response_data['re-id'] = column_id
                except:
                    response_data['status'] = False
            return HttpResponse(json.dumps(response_data), content_type="application/json")
        else:
            return redirect('/')


class ExportView(TemplateView):
    template_name = 'file_edit.html'

    def get_context_data(self, **kwargs):
        context = super(ExportView, self).get_context_data(**kwargs)
        context['files'] = File.objects.all()
        context['csrf_token_value'] = self.request.COOKIES['csrftoken']

        return context

    def post(self, request, *args, **kwargs):
        i = 0
        if request.is_ajax():
            response_data = {}
            method_type = request.POST.get('method_type')
            if method_type == "get_files":
                selected_files = request.POST.getlist('ids[]')
                try:
                    file = File.objects.filter(filename__in=selected_files)
                    # data = serializers.serialize("json", file)
                    response_data['status'] = True
                    response_data['demo_data'] = render_to_string('ajax/list_files.html',
                                                                  {'tags': file,'csrf_token_value': request.COOKIES['csrftoken']})
                except:
                    response_data['status'] = False

            elif request.POST.get('type') == "export":
                file_all_data = []
                for file_details in json.loads(request.POST.get('file_details')):
                    file = File.objects.get(id=file_details.get('id'))
                    col_ids = []
                    col_vals = []
                    all_data = []
                    for file_data in file_details.get('data'):
                        if file_data.get('col') and int(file_data.get('col')) not in col_vals:
                            col_vals.append(int(file_data.get('col')))
                    count = 0

                    for col_v in col_vals:
                        temp = col_vals[count]
                        row_data = []
                        for file_data in file_details.get('data'):
                            if file_data.get('col') and int(file_data.get('col')) == temp:
                                row_data.append(int(file_data.get('row'))-1)
                        count += 1
                        all_data.append({'cols':col_v,'rows':row_data})

                    for file_data in all_data:

                        # all_data.push({'col':file.columns.all()[int(file_data.get('col'))],''})
                        col_value = file_data.get('cols') - 1
                        rows_array = file_data.get('rows')
                        if file.columns.all()[col_value].id not in col_ids:
                            col_ids.append(file.columns.all()[col_value].id)
                            all_rows = list(file.columns.all()[col_value].row_data.all())
                            try:
                                filtered_rows = list(itemgetter(*rows_array)(all_rows))
                                row_temp = []
                                for rows in filtered_rows:
                                    row_temp.append(rows.value)
                                filtered_rows = row_temp
                            except:
                                filtered_rows = itemgetter(*rows_array)(all_rows).value
                            file_all_data.append({'cols':file.columns.all()[col_value].id,'rows':filtered_rows})

                to_rows = []
                temp_array = []
                col_uniq = []
                for file_data in file_all_data:
                    temp_array.append(file_data.get('cols'))
                to_rows.append(temp_array)

                total_row = 1


                for col_data in to_rows:
                    temp_array = []
                    for file_data in file_all_data:
                        try:
                            if type(file_data.get('rows')) == list:
                                if total_row < len(file_data.get('rows')):
                                    total_row = len(file_data.get('rows'))
                        except:
                            pass

                # total_row += len(to_rows)

                row_count = 0

                flag = 0

                for col_data in range(0,total_row):
                    temp_array = []

                    for file_data in file_all_data:

                        for col_data in to_rows[0]:

                            if file_data.get('cols') == col_data:

                                if type(file_data.get('rows')) == list:
                                    try:
                                        row = file_data.get('rows')[row_count]
                                        temp_array.append(row)

                                    except:
                                        temp_array.insert(row_count,' ')
                                else:
                                    row = file_data.get('rows')
                                    if row_count == 0:
                                        temp_array.append(row)
                                    else:
                                        temp_array.append(' ')

                    row_count += 1
                    flag += 1
                    to_rows.append(temp_array)
                with open('combine.csv', 'wb') as csvfile:
                    spamwriter = csv.writer(csvfile)
                    for data_objs in to_rows:
                        spamwriter.writerow(data_objs)


            return HttpResponse(json.dumps(response_data), content_type="application/json")

class DownloadView(TemplateView):
    template_name = "/"

    def get(self, request, *args, **kwargs):
        file_path = os.path.join(settings.BASE_DIR, 'combine.csv')
        if os.path.exists(file_path):
            with open(file_path, 'rb') as fh:
                response = HttpResponse(fh.read(), content_type="application/vnd.ms-excel")
                response['Content-Disposition'] = 'inline; filename=' + os.path.basename(file_path)
                return response
        raise Http404