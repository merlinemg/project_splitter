from django.contrib import admin

# Register your models here.
from app.models import Row, Column, File, Type

admin.site.register(Row)
admin.site.register(Column)
admin.site.register(File)
admin.site.register(Type)