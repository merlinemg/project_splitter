from __future__ import unicode_literals

from django.db import models

# Create your models here.

class Type(models.Model):
    type = models.CharField(max_length=40)

    def __str__(self):
        return self.type

class Row(models.Model):
    value = models.CharField(max_length=40,null=True,blank=True)
    is_exculded = models.BooleanField(default=False)


class Column(models.Model):
    column_header = models.CharField(max_length=40,null=True,blank=True)
    types = models.ForeignKey(Type, related_name='data_type',null=True,blank=True)
    row_data = models.ManyToManyField(Row, related_name='row_value',null=True,blank=True)
    no_of_rows = models.IntegerField(null=True,blank=True)



class File(models.Model):
    filename = models.CharField(max_length=40)
    columns = models.ManyToManyField(Column,related_name='file_column',null=True,blank=True)

