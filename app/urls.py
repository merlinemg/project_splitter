from django.conf.urls import url

from app.views import HomeView, ExportView, DownloadView

urlpatterns = [
    url(r'^$', HomeView.as_view(), name='home'),
    url(r'^export/', ExportView.as_view(), name='export'),
    url(r'^download/$', DownloadView.as_view(), name='download'),

]